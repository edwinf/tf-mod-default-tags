output "default_tags" {
  value = {
    "azure:resource:environment"  = "sandbox"
    "azure:resource:owner"        = "infrastructure"
  }
}